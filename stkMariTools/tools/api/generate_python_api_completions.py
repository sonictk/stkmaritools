#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module generate_python_api_completions: This module is a plugin for Mari
that allows the user to easily generate completion files for the Mari Python
API for use in IDEs. """

import logging
import mari
from stkMariTools.extras import generate_mari_autocompletions
from stkMariTools.lib.ui_utils import MariToolsMenuItem


class GeneratePythonAPICompletionsMenuItem(MariToolsMenuItem):
    """
    This class adds a **Generate Python API Completions** action.
    """

    logger = logging.getLogger(__name__)

    def __init__(self):
        """
        The constructor.

        :return:
        """
        super(GeneratePythonAPICompletionsMenuItem, self).__init__()

        mari.GeneratePythonAPICompletionsMenuItem = self

        self.actionIdentifier = 'Generate Mari Python API helpers'
        self.actionCommand = 'mari.GeneratePythonAPICompletionsMenuItem' \
                             '.generate_completions()'
        self.actionPath = 'MainWindow/&Help/&SDK/&Python'
        self.actionIcon = 'script'

        self.addMariToolsMenuItem()


    def generate_completions(self):
        """
        This method generates the completion files for the Mari Python API.

        :return: ``None``
        """

        self.logger.debug('Generating completion files...')

        generate_mari_autocompletions.generate_autocompletions()
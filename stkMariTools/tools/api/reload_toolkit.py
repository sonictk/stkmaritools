#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module reload_toolkit: This module is a plugin for Mari that allows for
reloading this entire toolkit from source from within Mari."""

import mari
import logging

from stkMariTools.lib.reload import Reloader
from stkMariTools.lib.ui_utils import MariToolsMenuItem


class ReloadToolkitMenuItem(MariToolsMenuItem):
    """
    This class adds a **Reload Mari Tools** action.
    """

    logger = logging.getLogger(__name__)

    def __init__(self):
        """
        The constructor.

        :return:
        """
        super(ReloadToolkitMenuItem, self).__init__()

        mari.ReloadToolkitMenuItem = self

        self.actionIdentifier = 'Reload Mari Tools'
        self.actionCommand = 'mari.ReloadToolkitMenuItem' \
                             '.reload()'
        self.actionPath = 'MainWindow/&Scripts/API'
        self.actionIcon = 'script'

        self.addMariToolsMenuItem()


    def reload(self):
        """
        This method reloads the toolkit from source.

        :return: ``None``
        """

        Reloader()
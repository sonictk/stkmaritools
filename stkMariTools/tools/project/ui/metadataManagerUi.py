# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Programs\stkmaritools\stkMariTools\tools\project\ui\metadataManagerUi.ui'
#
# Created: Sun Sep 20 19:24:11 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Form_metadataManager(object):
    def setupUi(self, Form_metadataManager):
        Form_metadataManager.setObjectName("Form_metadataManager")
        Form_metadataManager.resize(425, 585)
        self.verticalLayout = QtGui.QVBoxLayout(Form_metadataManager)
        self.verticalLayout.setObjectName("verticalLayout")
        self.grpBox_metadata = QtGui.QGroupBox(Form_metadataManager)
        self.grpBox_metadata.setObjectName("grpBox_metadata")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.grpBox_metadata)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.lbl_currentProject = QtGui.QLabel(self.grpBox_metadata)
        self.lbl_currentProject.setObjectName("lbl_currentProject")
        self.verticalLayout_2.addWidget(self.lbl_currentProject)
        self.lbl_projectName = QtGui.QLabel(self.grpBox_metadata)
        self.lbl_projectName.setText("")
        self.lbl_projectName.setObjectName("lbl_projectName")
        self.verticalLayout_2.addWidget(self.lbl_projectName)
        self.tableWidget_metadataView = MetadataManagerTableWidget(self.grpBox_metadata)
        self.tableWidget_metadataView.setObjectName("tableWidget_metadataView")
        self.tableWidget_metadataView.setColumnCount(0)
        self.tableWidget_metadataView.setRowCount(0)
        self.verticalLayout_2.addWidget(self.tableWidget_metadataView)
        self.btn_addMetadata = QtGui.QPushButton(self.grpBox_metadata)
        self.btn_addMetadata.setObjectName("btn_addMetadata")
        self.verticalLayout_2.addWidget(self.btn_addMetadata)
        self.btn_reset = QtGui.QPushButton(self.grpBox_metadata)
        self.btn_reset.setObjectName("btn_reset")
        self.verticalLayout_2.addWidget(self.btn_reset)
        self.btn_removeCustomMetadata = QtGui.QPushButton(self.grpBox_metadata)
        self.btn_removeCustomMetadata.setObjectName("btn_removeCustomMetadata")
        self.verticalLayout_2.addWidget(self.btn_removeCustomMetadata)
        self.btn_refresh = QtGui.QPushButton(self.grpBox_metadata)
        self.btn_refresh.setObjectName("btn_refresh")
        self.verticalLayout_2.addWidget(self.btn_refresh)
        self.grpBox_serialization = QtGui.QGroupBox(self.grpBox_metadata)
        self.grpBox_serialization.setObjectName("grpBox_serialization")
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.grpBox_serialization)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.btn_exportMetadata = QtGui.QPushButton(self.grpBox_serialization)
        self.btn_exportMetadata.setObjectName("btn_exportMetadata")
        self.verticalLayout_3.addWidget(self.btn_exportMetadata)
        self.btn_importMetadata = QtGui.QPushButton(self.grpBox_serialization)
        self.btn_importMetadata.setObjectName("btn_importMetadata")
        self.verticalLayout_3.addWidget(self.btn_importMetadata)
        self.verticalLayout_2.addWidget(self.grpBox_serialization)
        self.verticalLayout.addWidget(self.grpBox_metadata)
        self.btn_close = QtGui.QPushButton(Form_metadataManager)
        self.btn_close.setObjectName("btn_close")
        self.verticalLayout.addWidget(self.btn_close)

        self.retranslateUi(Form_metadataManager)
        QtCore.QMetaObject.connectSlotsByName(Form_metadataManager)

    def retranslateUi(self, Form_metadataManager):
        Form_metadataManager.setWindowTitle(QtGui.QApplication.translate("Form_metadataManager", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.grpBox_metadata.setTitle(QtGui.QApplication.translate("Form_metadataManager", "Project Metadata", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_currentProject.setText(QtGui.QApplication.translate("Form_metadataManager", "Current Project:", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_addMetadata.setText(QtGui.QApplication.translate("Form_metadataManager", "Add metadata entry", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_reset.setText(QtGui.QApplication.translate("Form_metadataManager", "Reset to default", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_removeCustomMetadata.setText(QtGui.QApplication.translate("Form_metadataManager", "Remove non-standard entries", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_refresh.setText(QtGui.QApplication.translate("Form_metadataManager", "Refresh view", None, QtGui.QApplication.UnicodeUTF8))
        self.grpBox_serialization.setTitle(QtGui.QApplication.translate("Form_metadataManager", "Serialization", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_exportMetadata.setText(QtGui.QApplication.translate("Form_metadataManager", "Export metadata to file...", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_importMetadata.setText(QtGui.QApplication.translate("Form_metadataManager", "Import metadata from file", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_close.setText(QtGui.QApplication.translate("Form_metadataManager", "Close", None, QtGui.QApplication.UnicodeUTF8))

from stkMariTools.lib.tools.project.metadataManagerUiLib import MetadataManagerTableWidget

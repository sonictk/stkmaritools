#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module examplePlugin: This module is an example of how to create a plugin in Mari. """

# noinspection PyUnresolvedReferences
import mari
from PySide import QtGui
from stkMariTools.lib.ui_utils import MariToolsMenuItem, MariWidget


class ExamplePaletteItem(MariToolsMenuItem):
    """
    This class adds the action.
    """

    def __init__(self):
        """
        The constructor.

        :return:
        """

        # Call base constructor
        super(ExamplePaletteItem, self).__init__()

        # Set the class instance in order for the action command to be
        # able to be set in the namespace correctly.
        mari.ExamplePaletteItem = self

        # Action item is to be called in the Menu
        self.actionIdentifier = 'Launch Example Palette'

        # Python command to be run when action is executed from the Menu
        self.actionCommand = 'mari.ExamplePaletteItem.doIt()'

        # Path to the action in the Mari Menu
        self.actionPath = 'MainWindow/&Scripts/Examples'

        # Icon to use for the action
        self.actionIcon = 'About'

        # Register the plugin
        self.addMariToolsMenuItem()


    def doIt(self):

        ExampleWidget()


class ExampleWidget(MariWidget):
    """
    This class instantiates an example widget.
    """

    def __init__(self, title='Example Palette', widgetType='palette'):
        """
        The constructor.

        :return:
        """

        self.closeButton = None

        # Call base constructor
        super( ExampleWidget, self ).__init__(title=title, widgetType=widgetType)


    def defineUi(self):
        """
        This method should be overloaded to handle defining the actual UI interface.
        It is run before populateData() and makeConnections().

        :return:
        """

        layout = QtGui.QVBoxLayout()

        checkbox = QtGui.QCheckBox('Show title', self)

        button = QtGui.QPushButton('Push Me')
        self.closeButton = QtGui.QPushButton('Close')

        self.setGeometry(300, 300, 250, 150)

        layout.addWidget(checkbox)
        layout.addWidget(button)
        layout.addWidget(self.closeButton)

        self.widget_holder.setLayout(layout)


    def makeConnections(self, *args, **kwargs):
        """
        This method handles connecting UI widgets to callbacks.

        :param args:
        :param kwargs:
        :return:
        """

        self.closeButton.clicked.connect(self.hidePalette)

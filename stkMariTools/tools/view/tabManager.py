#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module tabManager:
This module contains classes for opening a tab manager to manage the
viewport tabs in Mari.
"""

import logging
import mari
from PySide.QtGui import QListWidgetItem, QListWidget

from stkMariTools.tools.view.ui.tabManagerUi import Ui_Form_tabManager
from stkMariTools.lib.ui_utils import MariToolsMenuItem, MariWidget


class TabManagerMenuItem(MariToolsMenuItem):
    """
    This class adds a Clear History action.
    """

    logger = logging.getLogger(__name__)

    def __init__(self):
        """
        The constructor.

        :return:
        """

        super(TabManagerMenuItem, self).__init__()

        # Set the class instance
        mari.TabManagerMenuItem = self

        # Action item is to be called in the Menu
        self.actionIdentifier = 'View Tab Manager'

        # Python command to be run when action is executed from the Menu
        self.actionCommand = 'mari.TabManagerMenuItem.startTabManager()'

        # Path to the action in the Mari Menu
        self.actionPath = 'MainWindow/&Scripts/View'

        # Icon to use for the action
        self.actionIcon = 'SinglePane'

        self.addMariToolsMenuItem()


    def startTabManager(self):

        self.logger.debug('Opening Tab Manager...')

        TabManager()


class TabManager(MariWidget):
    """
    This class instantiates a Tab Manager widget that allows the user
    to manage their view tabs in Mari.
    """


    def __init__( self, title='Tab Manager'):
        """
        The constructor.

        :return:
        """

        self.ui_form = None

        # Call base constructor
        super( TabManager, self ).__init__(title=title)


    def defineUi(self):
        """
        This method should be overloaded to handle defining the actual UI interface.
        It is run before populateData() and makeConnections().

        :return:
        """

        # Get instance of UI form and setup widgets onto that instance
        self.ui_form = Ui_Form_tabManager()
        self.ui_form.setupUi(self.widget_holder)


    def makeConnections(self, *args, **kwargs):
        """
        Connects the UI controls to callback signals.

        :param args:
        :param kwargs:
        :return:
        """

        # Callback connections
        mari.utils.connect(mari.projects.opened, self.refreshTabList)

        # UI button connections
        self.ui_form.btn_closeTab.clicked.connect(self.closeTab)

        # Close button
        self.ui_form.btn_close.clicked.connect(self.cancelAction)


    def populateData(self, *args, **kwargs):
        """
        Populates data to the UI.

        Fills the tab list with the current list of tabs.

        :param args:
        :param kwargs:
        :return:
        """

        self.refreshTabList()


    def refreshTabList(self):
        """
        This method refreshes the currently available tabs in the list.

        :return:
        """

        assert isinstance(self.ui_form.listWidget_tabs, QListWidget), \
            '### Invalid QListWidget!!!'

        # Clear the list first to ensure fresh update
        self.ui_form.listWidget_tabs.clear()

        # Get current open tabs
        current_tabs = mari.app.tabNames()

        # Add items to the list widget
        for tab in current_tabs:
            list_item = QListWidgetItem(tab)
            self.ui_form.listWidget_tabs.addItem(list_item)


    def resetTabs(self):
        """
        This method resets the current tab layout in Mari to the layout when it
        was first opened.

        :return:
        """



        self.refreshTabList()


    def closeTab(self):
        """
        This method closes the tab selected in the listView.

        :return:
        """

        # Get the currently selected tab
        current_item = self.ui_form.listWidget_tabs.currentItem()

        if current_item:
            # Remove the selected tab
            mari.app.removeTab(current_item.text())

        self.logger.info('Removed the tab:{0}!'.format(current_item.text()))

        self.refreshTabList()
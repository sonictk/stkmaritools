# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Programs\stkmaritools\stkMariTools\tools\view\ui\tabManagerUi.ui'
#
# Created: Tue Dec 01 02:47:10 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Form_tabManager(object):
    def setupUi(self, Form_tabManager):
        Form_tabManager.setObjectName("Form_tabManager")
        Form_tabManager.resize(216, 316)
        self.verticalLayout = QtGui.QVBoxLayout(Form_tabManager)
        self.verticalLayout.setObjectName("verticalLayout")
        self.grpBox_tabManager = QtGui.QGroupBox(Form_tabManager)
        self.grpBox_tabManager.setObjectName("grpBox_tabManager")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.grpBox_tabManager)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.listWidget_tabs = QtGui.QListWidget(self.grpBox_tabManager)
        self.listWidget_tabs.setObjectName("listWidget_tabs")
        self.verticalLayout_2.addWidget(self.listWidget_tabs)
        self.btn_closeTab = QtGui.QPushButton(self.grpBox_tabManager)
        self.btn_closeTab.setObjectName("btn_closeTab")
        self.verticalLayout_2.addWidget(self.btn_closeTab)
        self.btn_resetTabs = QtGui.QPushButton(self.grpBox_tabManager)
        self.btn_resetTabs.setObjectName("btn_resetTabs")
        self.verticalLayout_2.addWidget(self.btn_resetTabs)
        self.verticalLayout.addWidget(self.grpBox_tabManager)
        self.btn_close = QtGui.QPushButton(Form_tabManager)
        self.btn_close.setObjectName("btn_close")
        self.verticalLayout.addWidget(self.btn_close)

        self.retranslateUi(Form_tabManager)
        QtCore.QMetaObject.connectSlotsByName(Form_tabManager)

    def retranslateUi(self, Form_tabManager):
        Form_tabManager.setWindowTitle(QtGui.QApplication.translate("Form_tabManager", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.grpBox_tabManager.setTitle(QtGui.QApplication.translate("Form_tabManager", "Open Tabs", None, QtGui.QApplication.UnicodeUTF8))
        self.listWidget_tabs.setToolTip(QtGui.QApplication.translate("Form_tabManager", "Shows the current list of open tabs.", None, QtGui.QApplication.UnicodeUTF8))
        self.listWidget_tabs.setStatusTip(QtGui.QApplication.translate("Form_tabManager", "Shows the current list of open tabs.", None, QtGui.QApplication.UnicodeUTF8))
        self.listWidget_tabs.setWhatsThis(QtGui.QApplication.translate("Form_tabManager", "Shows the current list of open tabs.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_closeTab.setToolTip(QtGui.QApplication.translate("Form_tabManager", "Closes the currently selected tab.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_closeTab.setStatusTip(QtGui.QApplication.translate("Form_tabManager", "Closes the currently selected tab.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_closeTab.setWhatsThis(QtGui.QApplication.translate("Form_tabManager", "Closes the currently selected tab.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_closeTab.setText(QtGui.QApplication.translate("Form_tabManager", "Close tab", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_resetTabs.setToolTip(QtGui.QApplication.translate("Form_tabManager", "Resets the tab layout to the Mari defaults.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_resetTabs.setStatusTip(QtGui.QApplication.translate("Form_tabManager", "Resets the tab layout to the Mari defaults.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_resetTabs.setWhatsThis(QtGui.QApplication.translate("Form_tabManager", "Resets the tab layout to the Mari defaults.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_resetTabs.setText(QtGui.QApplication.translate("Form_tabManager", "Reset tabs", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_close.setToolTip(QtGui.QApplication.translate("Form_tabManager", "Closes this window.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_close.setStatusTip(QtGui.QApplication.translate("Form_tabManager", "Closes this window.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_close.setWhatsThis(QtGui.QApplication.translate("Form_tabManager", "Closes this window.", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_close.setText(QtGui.QApplication.translate("Form_tabManager", "Close", None, QtGui.QApplication.UnicodeUTF8))


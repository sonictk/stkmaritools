#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module clearHistory: This module is a plugin for Mari that allows the user
to clear the history and cache from the current Project. """

import logging
import mari
import traceback
from PySide.QtGui import QMessageBox

from stkMariTools.lib.callbacks import ProjectChangedSubscriber, \
    ProjectChangedPublisher
from stkMariTools.lib.ui_utils import MariToolsMenuItem


class ClearHistoryMenuItem(MariToolsMenuItem):
    """
    This class adds a Clear History action.
    """

    logger = logging.getLogger(__name__)

    def __init__(self):
        """
        The constructor.

        :return:
        """
        super(ClearHistoryMenuItem, self).__init__()

        mari.ClearHistoryMenuItem = self

        self.actionIdentifier = 'Clear cached history'
        self.actionCommand = 'mari.ClearHistoryMenuItem.clearHistory()'
        self.actionPath = 'MainWindow/&Scripts/&Cache'
        self.actionIcon = 'Clear'

        self.addMariToolsMenuItem()

        # Subscribe to project change events and disable/enable the menu item
        # as necessary
        self.project_changed_publisher = ProjectChangedPublisher()

        self.project_changed_subscriber = ClearHistoryMenuItemSubscriber(
            self.project_changed_publisher
        )


    def clearHistory(self):
        """
        This method clears the Mari undo stack and cache.

        :return:
        """

        try: mari.history.clear()
        except RuntimeError:
            self.logger.error('### Could not clear the project history!!!\n{0}'
                              .format(traceback.print_exc()))
            # Display user prompt
            mari.utils.message(text='Could not clear the project history!\n'
                               'Check if there is no project open, '
                               'or if the current project requires saving.',
                               title='Could not clear project history!',
                               icon=QMessageBox.Icon.Warning)

            return

        mari.ddi.garbageCollect()
        mari.ddi.clearMemoryCache()


class ClearHistoryMenuItemSubscriber(ProjectChangedSubscriber):
    """
    Subscribes to project changed events.
    """

    # todo: implement this functionality

    def project_closed(self):
        self.logger.debug('A project closed')

    def project_opened(self):
        self.logger.debug('A project opened')
#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module docs: This module is a plugin for Mari that allows the user to
open the documentation for this toolkit."""

import logging
import webbrowser
import os
import mari

from stkMariTools.lib.ui_utils import MariToolsMenuItem


class ViewDocumentationMenuItem(MariToolsMenuItem):
    """
    This class adds a **View Documentation** action.
    """

    logger = logging.getLogger(__name__)

    def __init__(self):
        """
        The constructor.

        :return:
        """
        super(ViewDocumentationMenuItem, self).__init__()

        mari.ViewDocumentationMenuItem = self

        self.actionIdentifier = 'View Help (stkMariTools)'
        self.actionCommand = 'mari.ViewDocumentationMenuItem.view_documentation()'
        self.actionPath = 'MainWindow/&Help'
        self.actionIcon = 'Help'

        self.addMariToolsMenuItem()


    def view_documentation(self):
        """
        This method opens the documentation for this toolkit.

        :return: ``None``
        """

        # Get path to the documentation
        docs_index_path = os.path.join(
            os.path.dirname(
                os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.abspath(__file__)
                        )
                    )
                )
            ),
            'docs', 'build', 'html', 'index.html'
        )

        # If documentation does not exist locally, fallback to
        # use web address instead
        if not os.path.isfile(docs_index_path):
            docs_index_path = 'http://docs.sonictk.com/stkmaritools/build/html/'

        webbrowser.open(docs_index_path)
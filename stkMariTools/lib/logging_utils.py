"""
Module description.
"""

import logging

#todo: make the logger class auto add the handler upon creating an instance of it
class stkMariLogger(logging.Handler):
    """
    This class is a custom logger for the Mari Tools.
    """

    def __init__(self, stream=None):
        """
        The constructor.

        :param stream:
        """
        super(stkMariLogger, self).__init__(stream)
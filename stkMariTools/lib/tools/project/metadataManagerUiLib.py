#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module metadataManagerUiLib.py: This module contains classes that provide
additional functionality to the UI widgets in the Metadata Manager tool."""

from stkMariTools.lib.widget_utils import MariQTableWidget


class MetadataManagerTableWidget(MariQTableWidget):
    """
    This subclass of ``MariQTableWidget`` provides additional methods for
    working with metadata tables specifically in Mari.
    """

    def __init__( self, *args, **kwargs ):
        """
        The constructor.

        :param args:
        :param kwargs:
        :return: ``None``
        """
        super( MetadataManagerTableWidget, self ).__init__( *args, **kwargs )


    def mouseDoubleClickEvent(self, event):
        """
        Override the base method in order to catch double-clicks.
        Will create a new row if double-clicking on empty space in the table.

        :param event:
        :return: ``None``
        """

        super(MetadataManagerTableWidget, self).mouseDoubleClickEvent(event)
#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module app_utils: This module contains useful methods for dealing with Mari. """

import mari
import logging
from PySide.QtGui import QApplication


class MariAppUtils(object):
    """
    This class contains useful methods for interacting with the Mari application.
    """

    #: Defining Mari version release constants
    MARI_2_6v3_VERSION_NUMBER = 20603300 # MARI 2.6v3
    MARI_3_0v1_VERSION_NUMBER = 30001210 # MARI 3.0v1b10


    @classmethod
    def checkSupportedMariVersion(cls, mari_version='MARI_2_6v3_VERSION_NUMBER'):
        """
        This method checks for a Mari version that is supported by the plugin.

        :param mari_version: ``str`` or ``int`` that is constant defining the
            version of Mari to check against.

        return: ``bool`` determining if the current Mari version is supported.

        rtype: ``bool``
        """

        current_mari_version = mari.app.version().number()

        if isinstance(mari_version, str) and mari_version in dir(cls):
            mari_version = getattr(cls, mari_version)

        if current_mari_version >=  mari_version:
            return True
        else:
            return False


    @classmethod
    def getMariQApplicationInstance(cls):
        """
        This method returns the current Mari QApplication instance.

        :return: ``PySide.QtCore.QApplication`` object that is the Mari top-level instance.
        :rtype: ``PySide.QtCore.QApplication``
        """

        # noinspection PyArgumentList
        app = QApplication.instance

        if not app:
            app = QApplication([])

        return app


class MariCallbackManager(object):
    """
    This class is a callback manager for Mari that allows for global management 
    of Mari-specific callbacks.
    """

    def __init__(self):
        """
        The constructor.
        """

        self.callbacks = {}
        self.logger = logging.getLogger(__name__)

        
    def connect(self, signal, slot):
        """
        This method connects a given callback to its corresponding slot.

        :param signal: ``QSignal`` that is used for connection.
        :param slot: ``QSlot`` response that is meant to be called in response.
        """
        try: mari.utils.connect(signal, slot)
        except TypeError as err:
            raise TypeError('Incorrect type of signal/slot provided!\n{0}'.format(err))

        self.callbacks[signal] = slot
        self.logger.debug('Successfully connected: {0} to: {1}!'.format(signal, slot))

        
    def disconnect(self, signal, slot):
        """
        This method disconnects an existing callback from its response slot.

        :param signal: ``QSignal`` that is intended to be disconnected.
        :param slot: ``QSlot`` that is currently connected to the signal.

        """
        try: mari.utils.disconnect(signal, slot)
        except TypeError as err:
            raise TypeError('Incorrect type of signal/slot provided!\n{0}'.format(err))

        self.callbacks.pop(signal)
        self.logger.debug('Successfully disconnected: {0} from {1}'.format(signal, slot))

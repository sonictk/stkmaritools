#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module widget_utils: This module contains classes that define custom UI widgets
 used in Mari. """

import logging
from PySide.QtGui import QTableWidget, QKeySequence, QApplication


class MariQTableWidget(QTableWidget):
    """
    This subclass of ``QTableWidget`` provides additional features for use in Mari.
    """

    def __init__( self, parent=None, *args, **kwargs ):

        self.logger = logging.getLogger(__name__)

        # Call base class constructor
        super( MariQTableWidget, self ).__init__( parent, *args, **kwargs )


    def keyPressEvent(self, event):
        """
        Overrides the base class method to catch key events.
        Catch copy shortcut key in order to implement custom clipboard copy method.

        :param event:
        :return: ``None``
        """

        # Custom copy/paste behaviour
        if event.matches(QKeySequence.Copy):
            self.copyTableData()

        else:
            # Return base class implementation instead
            super(MariQTableWidget, self).keyPressEvent(event)


    def copyTableData(self):
        """
        This method copies the current data selected to the clipboard.

        :return:
        """

        # Get currently selected indexes
        selected_indexes = self.selectionModel().selectedRows()

        if selected_indexes:
            text = ''

            for i in selected_indexes:
                row = i.row()

                for col in range(self.columnCount()):
                    item = self.item(row, col)

                    if item:
                        # Add to string to return and format it
                        text += item.text()
                        text += '\t'

                    text += '\n'

                # Set formatted text to the clipboard
                # noinspection PyArgumentList
                QApplication.clipboard().setText(text)



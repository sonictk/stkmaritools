#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module cliUtils: This module provides useful methods for dealing
with the Command Line interface.
"""

import logging
from argparse import ArgumentParser


class ParseArgs(object):
    """
    This class contains methods for parsing arguments from the command line.
    Can be overloaded to provide custom command line arguments.
    """

    def __init__(self, description=None, *args):
        """
        Reads in the command line arguments passed by the user.

        :param description: ``str`` containing help about the module being run.
        :param args: ``str`` arguments that indicate what additional arguments are
                    to be read in.

                    Currently supports:

                        * *mode*
                        * *files*
                        * *output*
                        * *overwriteExisting*

        :return: ``None``
        """

        # Read in command-line arguments
        self.parser = ArgumentParser(description=description)

        self.addArguments(*args)

        self.args = self.parser.parse_args()


    def addArguments(self, *args):
        """
        This method adds/configures the available command line arguments.
        Can be overloaded for adding custom arguments.

        :return: ``None``
        """

        # Add common arguments
        self.parser.add_argument('-m', '--mode',
                                 help='Determines the mode in which the program '
                                    'should be run in.',
                                 type=str,
                                 action='store',
                                 dest='mode'

         )

        if 'files' in args:
            self.parser.add_argument('-f', '--files',
                                     help='Provide a semicolon-separated list of filepaths.',
                                     type=str,
                                     action='store',
                                     dest='files'
            )

        if 'output' in args:
            self.parser.add_argument('-o', '--output',
                                     help='Provide an output path.',
                                     type=str,
                                     action='store',
                                     dest='files'
            )

        if 'overwrite' in args:
            self.parser.add_argument('-O', '--overwriteExisting',
                                     help='Determines if existing files in '
                                          'output path are to be overwritten.',
                                     type=bool,
                                     action='store',
                                     dest='overwriteExisting'
            )


    def getLoggingLevel(self):
        """
        This retrieves the desired logging level. By default is set to INFO level.

        :return: ``int`` corresponding to chosen logging level
        :rtype: ``int``
        """

        try:
            loggingLevel = self.args.mode

            if loggingLevel == 'critical' or loggingLevel == 'CRITICAL':
                loggingLevel = logging.CRITICAL

            elif loggingLevel == 'error' or loggingLevel == 'ERROR':
                loggingLevel = logging.ERROR

            elif loggingLevel == 'warning' or loggingLevel == 'WARNING':
                loggingLevel = logging.WARNING

            elif loggingLevel == 'info' or loggingLevel == 'INFO':
                loggingLevel = logging.INFO

            elif loggingLevel == 'debug' or loggingLevel == 'DEBUG':
                loggingLevel = logging.DEBUG

            else:
                loggingLevel = logging.INFO

        except AttributeError:
            loggingLevel=logging.INFO

        return loggingLevel


    def getFiles(self):
        """
        This method retrieves a semicolon-separated list of files passed in as arguments.
        Returns a ``list`` of the file paths.

        :return: ``list`` of file paths
        :rtype: ``list``
        """

        try:
            files = self.args.files
            files = files.split(';')

        except AttributeError:
            files = None

        return files


    def getOutputPath(self):
        """
        This method returns the output path that is passed in as an argument.

        :return: ``str`` to file path
        :rtype: ``str``
        """

        try: outputPath = self.args.output
        except AttributeError: outputPath = None

        return outputPath


    def getOverwriteExisting(self):
        """
        This method returns the value that was passed for choosing to
        overwrite existing files in place.

        Defaults to ``True``.

        :return: ``bool``
        :rtype: ``bool``
        """

        try: overwriteExisting = self.args.overwriteExisting
        except AttributeError: overwriteExisting = True

        return overwriteExisting
#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module pyside_utils: This module contains classes useful for dealing with
PySide in Mari.
"""

import mari

from PySide.QtGui import QApplication


def get_mari_main_window():
    """
    This method returns the main Mari ``QMainWindow`` instance as a ``QWidget``.

    :return: ``QWidget``
    """

    # Activate the Mari window to be in focus
    mari.app.activateMainWindow()

    mari_main_window = QApplication.activeWindow()

    return mari_main_window
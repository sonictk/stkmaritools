#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module reload: This module contains methods for reloading the entire toolkit
from within the host application.
"""

import logging
import os


class Reloader(object):
    """
    This class contains methods for reloading the toolkit from source.
    """

    #: Get name of the top-level module name to be reloaded.
    toolkit_module_name = os.path.basename(
        os.path.dirname(
            os.path.dirname(os.path.abspath(__file__))
        )
    )


    def __init__(self, name=toolkit_module_name):
        """
        The constructor.

        :param name: ``str`` containing name of the top-level root module to reload.
            Defaults to the name of the top-level module based on the package
            name in the filesystem.
        """

        self.logger = logging.getLogger(__name__)

        self.reloadModule(name)


    def reloadModule(self, name, *args):
        """
        This method reloads the toolkit.

        :param name: ``str`` containing name of the top-level root module to reload.
        """
        module = __import__(name, globals(), locals(), ['*'], -1)

        path = module.__path__[0]

        self.__reloadRecursive(path, name)

        self.logger.info('Successfully reloaded all modules!')


    def __reloadRecursive(self, path, parent_name):
        """
        This method recursively reloads all modules and sub-modules from a given
        path and parent.

        :param path: ``str`` path to top-level module to reload files from.
        :param parent_name: ``str`` name of the top-level parent module.
        """

        for root, dirs, files in os.walk(path, True, None):

            # Parse all the files of given path and reload python modules
            for f in files:

                if f.endswith('.py'):
                    if f == '__init__.py':
                        name = parent_name
                    else:
                        name = parent_name + '.' + f[:-3]

                    self.logger.debug('Reloaded module : {0}'.format(name))

                    try:
                        module = __import__(name, globals(), locals(), ['*'], -1)
                        reload(module)

                    except ImportError, e:
                        for arg in e.args:
                            self.logger.debug(arg)

                    except Exception, e:
                        for arg in e.args:
                            self.logger.debug(arg)

            # Now reload sub modules as well
            for dir_name in dirs:
                self.__reloadRecursive(
                    os.path.join(path, dir_name), parent_name+'.'+dir_name
                )

            break
.. _about:

####################
About the Mari Tools
####################

Description
===========

This is the documentation for the Mari Tools developed by sonictk.

The Mari Tools are a collection of tools developed by Siew Yi Liang that include
several useful utilties, such as incremental file saving, project metadata
management, along other plugins. It also allows for easy authoring of custom plugins
and palettes through a auto-module registration system.


Release Notes
=============

Please refer to the :ref:`release` for more details.


.. _about-the-author:

About the author
================

Siew Yi Liang

Please contact me via email at:

admin@sonictk.com
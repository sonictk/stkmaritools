..
    :copyright: Copyright (c) 2015 sonictk-Frostburn

.. _getting-started:


***************
Getting Started
***************

In order to work on the Mari Tools, you will need a
working knowledge of Python. If you are already familiar with Python,
please take a moment to review the :ref:`codingStyleGuide`.

First, please follow the instructions for :ref:`manual-installation` in
order to get a working copy of the source.


Making your first plugin using the Mari Tools
=============================================

.. tip::

    Example source code described here is also available at:
    ``./stkMariTools/tools/examples``.

.. tip::

    Please refer to the :ref:`api-reference` if you are unfamiliar with the classes
    being described here.

To begin making your own Mari plugins and have them be automatically registered using
the framework in this toolkit, you will need to perform the following steps:

1. Subclass ``MariToolsMenuItem`` and override the constructor to provide details
   about the plugin.

   .. warning::

        Do not duplicate plugin identifiers or action paths! This will cause
        only one of the plugins to be registered on Mari startup.

2. Include a ``actionCommand`` instance variable that stores a string that will
   be the command executed when the plugin is executed. This can be anything from
   running mari commands to opening a full-blown PySide UI. Here we will discuss
   how to perform the latter.

Example subclass:

.. literalinclude:: ../../../stkMariTools/tools/examples/examplePlugin.py
    :language: python
    :pyobject: ExampleMenuItem


3. After this, you should subclass ``MariQWidget`` in order to create your UI class.

.. literalinclude:: ../../../stkMariTools/tools/examples/examplePlugin.py
    :language: python
    :pyobject: ExampleWidget

4. You should override ``defineUi`` in order to define your own widget's UI. You
   can either do so by defining the widgets within the method directly or by
   importing a generated .py file from Qt Designer as well.

5. ``makeConnections`` should be overriden to handle connecting/disconnecting all
   QSignal/QSlots within the UI itself.

6. Finally, ``self.hidePalette`` and ``self.cancelAction`` methods are available
   as helper methods to destroy the UI, and should be used to connect to UI actions
   that are intended to close the plugin for palettes/plugins respectively.


Registering the plugin
======================

Once you have written the plugin, you can place it under:

``stkMariTools/tools/<path-to-action>``

And it should automatically register the next time you launch Mari.

.. warning::

    While it is not strictly required that ``self.actionPath`` match the exact
    folder structure that you have registered the plugin at, it is **highly
    recommended** that you do so in order to minimize confusion and make it
    easier to debug your plugin.


What's next?
============

Have a look at the :ref:`api-reference` to see what other plugins are available,
what other libraries are available, and to see where best you could help to
contribute to the toolkit!
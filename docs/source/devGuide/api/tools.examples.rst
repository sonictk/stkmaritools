tools.examples package
======================

Submodules
----------

tools.examples.examplePalette module
------------------------------------

.. automodule:: tools.examples.examplePalette
    :members:
    :undoc-members:
    :show-inheritance:

tools.examples.examplePlugin module
-----------------------------------

.. automodule:: tools.examples.examplePlugin
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: tools.examples
    :members:
    :undoc-members:
    :show-inheritance:

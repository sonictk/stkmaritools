lib.tools.project package
=========================

Submodules
----------

lib.tools.project.metadataManagerUiLib module
---------------------------------------------

.. automodule:: lib.tools.project.metadataManagerUiLib
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: lib.tools.project
    :members:
    :undoc-members:
    :show-inheritance:

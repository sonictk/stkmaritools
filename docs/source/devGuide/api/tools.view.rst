tools.view package
==================

Subpackages
-----------

.. toctree::

    tools.view.ui

Submodules
----------

tools.view.tabManager module
----------------------------

.. automodule:: tools.view.tabManager
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: tools.view
    :members:
    :undoc-members:
    :show-inheritance:

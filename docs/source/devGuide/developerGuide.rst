#################
Developer's Guide 
#################

This is the developer's guide for the Mari Tools.

.. toctree::
    :maxdepth: 2

    getting-started
    codingStyleGuide


.. _api-reference:

API Reference
=============

This is the API reference for the modules used in the Mari Tools.


Mari Tools Libraries
--------------------

Common Libraries
^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2
   :glob:

   api/lib*


Plugin Modules
^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2
   :glob:

   api/tools*



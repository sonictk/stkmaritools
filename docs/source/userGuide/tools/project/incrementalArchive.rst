.. _incremental-archive:

***************************
Incremental Archive Project
***************************

Description
===========

This plugin creates a new incremental archive version of the project automatically.

Usage
=====

.. note::

    This section is still a work in progress.

Execute the action from the menu.


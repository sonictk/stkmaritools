.. _metadata-manager:

****************
Metadata Manager
****************

Description
===========

This plugin helps manage the metadata associated with the current Mari project.

Usage
=====

Execute the action from the menu. You should see the **Metadata Manager** window
open:

.. figure:: ../../../_resources/images/tools/project/metadataManagerPreview.*
    :alt: Metadata Manager Preview

Below, each UI element will be described in greater detail.

Current Project
---------------

This lists the current project that is being inspected. This should be the
currently open project in the Mari viewport.

.. _metadata-view:

Metadata View
-------------

This lists the current metadata keys and values associated with the current
project. You can edit existing keys/values by simply editing the table values.

Reset to default
----------------

This will reset the project's metadata to the values that were initially set
when it was first opened.

Remove non-standard entries
---------------------------

All Mari projects come with some default metadata associated with them. This
will remove any extraneous user-added metadata keys/values that were added to
the project.

.. warning::

    Tools regularly rely on custom metadata that are added to projects in order
    to store project-specific data. Using this function may cause unexpected
    results with those tools!

Refresh View
------------

This refreshes the :ref:`metadata-view` manually, in case any values were
updated and the UI failed to refresh accordingly.

Serialization
-------------

Export metadata to file
^^^^^^^^^^^^^^^^^^^^^^^

This exports the current project metadata to a configuration file which can be
either used as a backup snapshot of the current project's metadata, or applied
to other Mari projects.

Import metadata from file
^^^^^^^^^^^^^^^^^^^^^^^^^

This imports an existing configuration file which is then immediately applied to
updating the current project's metadata keys/values.



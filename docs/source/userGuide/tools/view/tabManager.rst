.. _tab-manager:

***********
Tab Manager
***********

Description
===========

This plugin helps manage the currently open viewport tabs.


Usage
=====

Execute the action from the menu. You should see the **Tab Manager** window
open:

.. figure:: ../../../_resources/images/tools/view/tabManagerPreview.*
    :alt: Tab Manager Preview

Below, each UI element will be described in greater detail.

.. _open-tabs:

Open tabs
---------

This lists the current tabs that are open in the viewport.

Close Tab
---------

After selecting a tab from the :ref:`open-tabs`, execute this action in order to
close it and remove it from the viewport.


.. _clear-history:

*************
Clear History
*************

Description
===========

This plugin clears the current project history and cache as well in order to help
save system resources.

Usage
=====

Execute the action from the menu. If the project has not been saved, Mari should
prompt you to do so first.


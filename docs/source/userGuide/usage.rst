..
    :copyright: Copyright (c) 2015 sonictk-Frostburn

.. _usage:

#####
Usage
#####


Getting Started
===============

After installation, you should be able to see the new tools show up in the Mari
UI by going to the **Scripts** menubar item.


Tools
=====

Below is a list of all the available **tools** currently available for use.

Cache
-----

.. toctree::
   :maxdepth: 1
   :glob:

   tools/cache/*

Project
-------

.. toctree::
   :maxdepth: 1
   :glob:

   tools/project/*

View
----

.. toctree::
   :maxdepth: 1
   :glob:

   tools/view/*


Help
----

At any time, you can also view this documentation from within Mari by going to:

**Help > View Help (stkMariTools)**
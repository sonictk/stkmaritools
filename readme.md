# README #

This is the repository for Mari Tools by Siew Yi Liang.

### Requirements ###

Mari 2.6v5 or higher is required in order to use these tools.

### Installation ###

Please refer to the included documentation on how to install these tools.

Alternatively, if you do not have Sphinx and/or are unable to build the docs due to 
issues, please refer to a live version of the documentation available at: 

http://docs.sonictk.com/stkmaritools/build/html/

The live documentation may not always be updated to reflect the latest changes, so keep that in mind!

### Troubleshooting ###

Please file an issue on the [BitBucket issue tracker](https://bitbucket.org/sonictk/stkmaritools/issues?status=new&status=open).

### About the Author ###

Siew Yi Liang

admin@sonictk.com


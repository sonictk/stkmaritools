@echo off

set CURRENTDIR=%cd%

cd %PROGRAMFILES%\ImageMagick-6.9.1-Q16

echo %CURRENTDIR%

convert "%CURRENTDIR%\logo.png" -define icon:auto-resize=256,128,64,48,32,16 "%CURRENTDIR%\logo_fb.ico"
@echo off
REM
REM Build script for installer
REM

title Compiling Installer

set PATH=%PATH%;C:\Program Files (x86)\Inno Setup 5

echo "Current directory for command execution:"
echo %~dp0

iscc "%~dp0setup.iss"

echo "Reached end of installer build script!"
; This is the install script for the Mari Tools build for Inno Setup.

#define MyAppName "Mari Tools"
#define MyAppPublisher "sonictk"
#define MyAppURL "https://bitbucket.org/sonictk/stkmaritools"
#define MyAppContact "admin@sonictk.com"

; Read previous build number from INI config. If no build number exists, set to 0
#define MyAppVersion Int(ReadIni(SourcePath+"\\version.ini", "Info", "Build", "0"))

; Increment build number and write to INI
;#expr MyAppVersion = MyAppVersion + 1
;#expr WriteIni(SourcePath+"\\version.ini", "Info", "Build", MyAppVersion)

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)

AppId={{D895C655-25F8-4C11-BB91-0D9BFF0CFFEF}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
AppContact={#MyAppContact}
AppCopyright={#MyAppPublisher}

VersionInfoTextVersion=Build {#MyAppVersion}

DefaultDirName={pf}\{#MyAppPublisher}\{#MyAppName}
DefaultGroupName={#MyAppPublisher}
AllowNoIcons=yes
OutputBaseFilename=setup_r{#MyAppVersion}
Compression=lzma/ultra64
SolidCompression=yes
ShowTasksTreeLines=True
SetupLogging=yes
EnableDirDoesntExistWarning=True
MinVersion=0,5.01sp3
UsePreviousAppDir=False
AppendDefaultDirName=False
OutputDir="..\bin\"
ChangesEnvironment=True
CloseApplications=yes
DisableProgramGroupPage=auto
PrivilegesRequired=admin
SetupIconFile="..\resource\logo_fb.ico"
WizardImageFile="..\resource\wizard.bmp"
WizardSmallImageFile="..\resource\wizardSmall.bmp"
InfoBeforeFile="info.txt"
LicenseFile="LICENSE.txt"
Uninstallable=True
ArchitecturesInstallIn64BitMode="x64 ia64"
ArchitecturesAllowed="x64 ia64"
VersionInfoVersion={#MyAppVersion}
VersionInfoCompany={#MyAppPublisher}
VersionInfoDescription={#MyAppName}
VersionInfoCopyright={#MyAppPublisher}
VersionInfoProductName={#MyAppName}
VersionInfoProductVersion={#MyAppVersion}
VersionInfoProductTextVersion=Build {#MyAppVersion}
InternalCompressLevel=ultra64

[Registry]
; Append program directory to MARI_SCRIPT_PATH 
Root: "HKLM64"; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType: expandsz; ValueName: "MARI_SCRIPT_PATH"; ValueData: "{olddata};{app}"; Check: NeedsAddPath('{app}')

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "japanese"; MessagesFile: "compiler:Languages\Japanese.isl"
Name: "chinese_simplified"; MessagesFile: "compiler:Languages\ChineseSimplified.isl"
Name: "korean"; MessagesFile: "compiler:Languages\Korean.isl"

[Files]
; NOTE: Don't use "Flags: ignoreversion" on any shared system files
Source: "..\..\docs\*"; DestDir: "{app}\docs"; Flags: ignoreversion createallsubdirs recursesubdirs
Source: "..\..\stkMariTools\*"; DestDir: "{app}"; Flags: ignoreversion createallsubdirs recursesubdirs; Excludes: "*.pyc"
Source: "..\..\init_stkMariTools.py"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\readme.md"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\requirements.txt"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\{#MyAppName}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"

[ThirdParty]
UseRelativePaths=True

[Run]
Filename: "{app}\docs\build\html\index.html"; Flags: postinstall shellexec; Description: "View documentation";

[Components]

[Code]
// Global variables

// This function will return True if the Param already exists in the system PATH
function NeedsAddPath(Param: String): Boolean;
var
  OrigPath: String;

begin
    if not RegQueryStringValue(HKLM64, 'SYSTEM\CurrentControlSet\Control\Session Manager\Environment', 'MARI_SCRIPT_PATH', OrigPath) then 
    begin
        Result := True;
        exit;
    end;

    // look for the path with leading and trailing semicolon; Pos() returns 0 if not found
    Result := Pos(';' + Param + ';', ';' + OrigPath + ';') = 0;

end;


// This method checks for presence of uninstaller entries in the registry and returns the path to the uninstaller executable.
function GetUninstallString: String;

var
  uninstallerPath: String;
  uninstallerString: String;

begin
  Result := '';
  
  // Get the uninstallerPath from the registry
  uninstallerPath := ExpandConstant('SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{{D895C655-25F8-4C11-BB91-0D9BFF0CFFEF}_is1');
  
  uninstallerString := '';
  
  // Check if uninstaller entries in registry have values in them
  if not RegQueryStringValue(HKLM64, uninstallerPath, 'UninstallString', uninstallerString) then
    RegQueryStringValue(HKCU, uninstallerPath, 'UninstallString', uninstallerString);
  
  // Return path of uninstaller to run  
  Result := uninstallerString;

end;


// This method checks if a previous version has been installed
function PreviousInstallationExists : Boolean;
begin
  // Check if not equal '<>' to empty string and return result
  Result := (GetUninstallString() <> '');
end;


// This Event function runs before setup is initialized
function InitializeSetup(): Boolean;

var
  checkNetCxn : Boolean;
  uninstallChoiceResult: Boolean;
  uninstallPath : String;
  iResultCode : Integer;
  previouslyInstalledCheck : Boolean;

begin


  // Now check if previous version was installed
  previouslyInstalledCheck := PreviousInstallationExists;
  if previouslyInstalledCheck then
  begin
    uninstallChoiceResult := MsgBox('A previous installation was detected. Do you want to uninstall the previous version first? (Recommended)', mbInformation, MB_YESNO) = IDYES; 
    
    // If user chooses, uninstall the previous version and wait until it has finished before allowing installation to proceed
    if uninstallChoiceResult then
    begin
      uninstallPath := RemoveQuotes(GetUninstallString());
      Exec(ExpandConstant(uninstallPath), '', '', SW_SHOW, ewWaitUntilTerminated, iResultCode);

      Result := True;
    end

    else
    begin
      Result := True;
      Exit;
    end;
  end

  else
    Result := True;

end;     



